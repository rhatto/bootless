[[!meta title="Bootless: 'evil maid' (sic) mitigator"]]

Bootless is a scheme allowing a computer with encrypted disk to stay without
attached bootloader in order to make more difficult to tamper the initialization
process. Bootless is a bootloader installed in a removable media and used to
initialize computers.

It offers partial protection against the so-called [evil maid attacks][], which
basically consists in compromising the boot procedure on unnattended machines.

* [Repository](https://git.fluxo.info/bootless).
* [Tutorial](tutorial).
* [References](references).
* [TODO](todo).
* License: [GPLv3+](LICENSE).
* Contact: rhatto at riseup.net.

[evil maid attacks]: https://en.wikipedia.org/wiki/Evil_maid_attack

## Design

The user has at least one USB thumb drive which will be used to boot multiple
operating systems in multiple machines for multiple different projects/farms
(personal, work, hackerspace, etc). User keeps the bootless thumb drive in
a safe place protected from physical intrusions.

The `bootless` application wraps around `grub-mkrescue` to create a USB
bootdisk with preloaded custom configuration and optional kernel and initramfs
images.

## Dependencies

* [GNU Grub](https://www.gnu.org/software/grub/).
* Reference implementation is targeted to Debian like operating systems.
* Optionally use git and [git-annex](http://git-annex.branchable.com/) to
  manage your repository and images.

## Installation

Don't want to install another piece of software and prefer to build a bootless
rescue disk yourself? Then read about the [barebones tutorial](tutorial).

Just clone

    git clone https//git.fluxo.info/bootless

You can also verify the latest commit's OpenPGP signature:

    /usr/bin/git -C kvmx verify-commit HEAD

Note that `/usr/bin/git` is called to avoid any other `git` wrappers or aliases
you might have available on your shell.

You can optionally add the `bootless` script it to your `$PATH` environment
variable or package it to your preferred distro.

## Workflow

Initialize:

    bootless init <folder> [repository]

Create an image:

    boootless image <folder> [image]

Write image to thumb drive:

    boootless write <folder> <device>

Check device/image signatures:

    bootless check <folder> <device>

## Customization

* Place your custom images into `custom` folder.
* Edit `custom/custom.cfg` to suit your needs.

## Threat Model

### Does bootless mitigate all types of Evil Maid attacks? No.

1. It reduces the attack surface by placing the bootloader and images away from
   the physical machine and recommending you to use Full Disk Encryption (FDE)
   to store your operating system, swap and data.

2. Infection is still possible in plenty of unencrypted/unauthenticated
   software residing in the machine, such as BIOS, network firmware and
   potential backdoors such as Intel's AMT/ME. Nowadays seems like there
   are plenty of places were malicious code can be placed, not to say
   about the many ways miniaturized hardware can be implanted.

3. The USB stick itself is not a static device: it's has a built-in controller
   that could be exploited to present to your computer a compromised kernel or
   initramfs ([BadUSB attacks](https://links.fluxo.info/tags/badusb)).

Again:

* Bootless does not remove the threat of an evil tamperer but significantly
  reduces the attack surface. Exploits in the bootloader's `stage0` would need
  to be significantly smaller to do their jobs and optionally also load
  an operating system (in case it's intended not to be noticed).

* Spyware could still be installed in the firmware or in specialized hardware,
  which are plentiful in current computers.

### Additional mitigations

1. For physical attempts to tamper with your bare metal, you might try to
   protect and monitor your perimeter.

2. From inside threats such as preloaded backdoors in the hardware, the best
   you can do is to look for laboratory audits and build and use open hardware.

3. Check your boot using something like
   [anti-evil-maid](http://theinvisiblethings.blogspot.com.br/2011/09/anti-evil-maid.html)
   ([repository](https://github.com/QubesOS/qubes-antievilmaid)),
   [smartmonster](https://git.fluxo.info/smartmonster)
   ([original repository](https://github.com/ioerror/smartmonster)) or
   [chkboot](https://wiki.archlinux.org/index.php/Dm-crypt/Specialties#chkboot).

4. Against additional perimeter attacks, you could combine bootless with a
   solution like Edward Snowden's [Haven](https://guardianproject.github.io/haven/)
   or even always stay with your
   [TPC - Trusted Physical Console](https://web.archive.org/web/20180914153944/http://cmrg.fifthhorseman.net/wiki/TrustedPhysicalConsole).
   See also this [short video HOWTO](https://twitter.com/BlackAlchemySo2/status/1378565221879529472).

5. When turning on your machine, make sure that the ethernet and wireless
   networks are switched off (this could be done by removing cables, antennas
   or switching the "rfkill" button in laptops), preventing any bootloader exploit
   that to broadcast keystrokes.

6. Implement [Physically Unclonable Functions][] (PUF) at your device:
    * [Thwarting Evil Maid Attacks](https://media.ccc.de/v/30C3_-_5600_-_en_-_saal_1_-_201312301245_-_thwarting_evil_maid_attacks_-_eric_michaud_-_ryan_lackey#t=2616) (30C3).
    * [Don't Want Your Laptop Tampered With? Just Add Glitter Nail Polish](https://www.wired.com/2013/12/better-data-security-nail-polish/).
    * [Home-made tamper-evident security seals for kids and adults alike](http://blog.ssokolow.com/archives/2017/04/08/home-made-tamper-evident-security-seals-for-kids-and-adults-alike/).

[Physically Unclonable Functions]: https://en.wikipedia.org/wiki/Physical_unclonable_function
