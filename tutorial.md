[[!meta title="Bootless: barebones tutorial"]]

This tutorial helps you to build a Grub USB stick to boot your operating systems.

# Create basic folder structure

    mkdir -p bootless/boot/{grub,custom}
    wget     https://git.fluxo.info/bootless/plain/templates/grub.cfg   -O bootless/boot/grub/grub.cfg
    wget     https://git.fluxo.info/bootless/plain/templates/custom.cfg -O bootless/boot/custom/custom.cfg

# Customize

Please check and customize the downloaded configuration above. The defaults:

* Have examples to help customizations.
* Allows you to load into Grub and boot your OS using the grub shell.
* Provides two functions, `bootfde` and `bootimg` to simplify booting your
  encrypted system.

You may also place custom images into the USB stick -- with the drawback to
have to keep them up-to-date:

    cp -r /boot /tmp/bootless/custom/debian

# Create a bootable image

    grub-mkrescue -o bootless.iso bootless

# Record the image into the USB stick

**WARNING!** This will irrevocably replace the contents of your USB stick. Pay
attention to get the right device. If your USB stick is attached to your
computer and reachable through `/dev/sdb`, you just have to run

    sudo dd if=bootless.iso of=/dev/sdb
