[[!meta title="Bootless: references"]]

## GNU Grub

* [Bootable grub USB stick (EFI and BIOS for Intel)](http://debian-administration.org/users/dkg/weblog/112).
* [Grub2](https://help.ubuntu.com/community/Grub2) (Ubuntu Help).
* [GRUB2 Manual](http://grub.enbug.org/Manual) (Wiki).
* [Using GRUB to Set Up the Boot Process](http://www.linuxfromscratch.org/lfs/view/development/chapter08/grub.html).
* [GNU Grub Manual](http://www.gnu.org/software/grub/manual/grub.html).
* On `cryptopts`: ([1](http://www.c3l.de/linux/howto-completly-encrypted-harddisk-including-suspend-to-encrypted-disk-with-ubuntu-6.10-edgy-eft.html), [2](http://manpages.ubuntu.com/manpages/lucid/man8/initramfs-tools.8.html), [3](http://solvedlinuxissues.blogspot.com.br/2011/11/encrypted-ubuntu-filesystem-on-logical.html), [4](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=348147), [5](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=358452)), see `/usr/share/doc/cryptsetup/README.initramfs.gz` for details.

## Boot

* [Auto-booting and Securing a Linux Server with an Encrypted Filesystem](http://serverfault.com/questions/34794/auto-booting-and-securing-a-linux-server-with-an-encrypted-filesystem).
* [#348147 - Allow subscripts to alter ROOT (was: Add support for cryptoroot) - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=348147) ([crypt_root and real_root on gentoo](http://wiki.gentoo.org/wiki/Genkernel)).

## Images

* [How can I mount a disk image?](http://superuser.com/questions/344899/how-can-i-mount-a-disk-image).
* [GRUB 2 - OSDev](http://wiki.osdev.org/GRUB_2): instalando o grub em várias mídias distintas.
* [Disk mounting](http://www.noah.org/wiki/Disk_mounting).
* [Loop-mounting partitions from a disk image](http://madduck.net/blog/2006.10.20:loop-mounting-partitions-from-a-disk-image/).

## UEFI

* [gummiboot](http://freedesktop.org/wiki/Software/gummiboot/).
* [booting a self-signed Linux kernel | The Linux Foundation](http://www.linuxfoundation.org/news-media/blogs/browse/2013/09/booting-self-signed-linux-kernel).

## Security

* [implementing the evil maid attack on linux with Luks - Pollux's blog](https://www.wzdftpd.net/blog/index.php?post/2009/10/28/44-implementing-the-evil-maid-attack-on-linux-with-luks).

## Full Disk Encryption

* [Grub Crypt · Grub with crypto enhancements](http://grub.johnlane.ie/).
* [Yet Another Full Disk Encryption with Ubuntu 11.10 | On Science and Technology](https://archimedesden.wordpress.com/2011/10/21/yet-another-full-disk-encryption-with-ubuntu-11-10/).
* [MissingLink.xyz - Tutorial: GRUB2 Cryptomount](http://missinglink.xyz/grub2-bootloader/understanding-grub2-cryptomount/).
* [Ubuntu Full Disk Encryption (FDE) with encrypted /boot](http://missinglink.xyz/security/tutorial-debianubuntu-full-disk-encryption-luks-fde-including-encrypted-boot/)
* [Full disk encryption with LUKS (including /boot) · Pavel Kogan](http://www.pavelkogan.com/2014/05/23/luks-full-disk-encryption/).
* [Full-Crypto setup with GRUB2](http://michael-prokop.at/blog/2014/02/28/full-crypto-setup-with-grub2/)

## LUKS

* [Linux Unified Key Setup - Wikipedia](https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup)

### LUKS 2 and Argon2id

General information:

* mjg59: [PSA: upgrade your LUKS key derivation function][]
* [Une lettre d’Ivan, enfermé à la prison de Villepinte : perquisitions et disques durs déchiffrés » Indymedia Nantes](https://nantes.indymedia.org/posts/87395/une-lettre-divan-enferme-a-la-prison-de-villepinte-perquisitions-et-disques-durs-dechiffres/)
* [Password Storage - OWASP Cheat Sheet Series](https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html)
* [GitHub - CyberKnight00/Argon2_Cracker: Argon2 Hash Cracker](https://github.com/CyberKnight00/Argon2_Cracker)
* [An information theoretic model of privacy and security metrics — GNU MediaGoblin](https://media.libreplanet.org/u/libreplanet/m/an-information-theoretic-model-of-privacy-and-security-metrics/)
* [Debian -- Details of package grub-pc in bookworm](https://packages.debian.org/bookworm/grub-pc)

Relevant references:

* [Enable LUKS2 and Argon2 Support for Packages - Gentoo Configuration Guide: Full Disk LUKS2 with GRUB and systemd - Leo3418's Personal Site](https://leo3418.github.io/collections/gentoo-config-luks2-grub-systemd/packages.html)
* [Tails - Weak cryptographic parameters in LUKS1](https://tails.net/security/argon2id/index.en.html)

Bug reports:

* [GNU GRUB - Bugs: bug #55093, Add LUKS2 support](https://savannah.gnu.org/bugs/?55093)

Existing patches for GRUB:

* [AUR (en) - grub-improved-luks2-git](https://aur.archlinux.org/packages/grub-improved-luks2-git)
* [Support Argon2 KDF in LUKS2](https://lists.gnu.org/archive/html/grub-devel/2021-08/msg00027.html)
* [Re: GRUB 2.12 release - update](https://lists.gnu.org/archive/html/grub-devel/2022-11/msg00094.html)

Workarounds to use LUKS2 and Argon2id on boot devices:

* [How to install debian 12 with full disk (boot too) luks2 encryption grub2, lvm UEFI? - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/753886/how-to-install-debian-12-with-full-disk-boot-too-luks2-encryption-grub2-lvm-u)
* [Here's how to use grub2-git and argon2id for encrypted boot : r/NixOS](https://www.reddit.com/r/NixOS/comments/12wqedo/heres_how_to_use_grub2git_and_argon2id_for/)
* [encryption - GRUB alternative for LUKS2 with Argon2ID support - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/633713/grub-alternative-for-luks2-with-argon2id-support)
* [Enable LUKS2 and Argon2 support for Grub in Manjaro/Arch | Ming Di Leom's Blog](https://mdleom.com/blog/2022/11/27/grub-luks2-argon2/)

Systems that already support it (but nos as FDE):

* [Tails 5.14 is out! (2023-06-13) - General Discussion - Tor Project Forum](https://forum.torproject.org/t/tails-5-14-is-out-2023-06-13/7986)

[PSA: upgrade your LUKS key derivation function]: https://mjg59.dreamwidth.org/66429.html
