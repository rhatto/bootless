class bootless (
  $folder     = false,
  $repository = '',
  $owner      = 'root',
){
  file { '/usr/local/bin/bootless':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => 0755,
    source => 'puppet://modules/bootless/bootless',
  }

  if $folder != false {
    exec { "bootless-init":
      command => "/usr/local/bin/bootless init ${folder} ${repository}",
      user    => $owner,
      creates => $folder,
    }
  }
}
