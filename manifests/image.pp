define bootless::image(
  $path,
) {
  exec { "bootless-image-${name}":
    command => "/usr/local/bin/bootless image ${::bootless::folder} ${path}"
    owner   => ${::bootless::owner},
    require => Exec['bootless-init'],
  }
}
