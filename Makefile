#
#  Bootless Makefile by Silvio Rhatto (rhatto at riseup.net).
#
#  This Makefile is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation; either version 3 of the License, or any later version.
#
#  This Makefile is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
#  Place - Suite 330, Boston, MA 02111-1307, USA
#

PACKAGE  = bootless
VERSION  = $(shell ./bootless | head -n 1 | cut -d ' ' -f 2)
PREFIX  ?= /usr/local
ARCHIVE ?= tarballs
INSTALL  = /usr/bin/install

clean:
	find . -name *~ | xargs rm -f # clean local backups

install_bin:
	$(INSTALL) -D --mode=0755 bootless $(DESTDIR)/$(PREFIX)/bin/bootless

install_doc:
	$(INSTALL) -D --mode=0644 index.mdwn $(DESTDIR)/$(PREFIX)/share/doc/$(PACKAGE)/README.md
	$(INSTALL) -D --mode=0644 LICENSE $(DESTDIR)/$(PREFIX)/share/doc/$(PACKAGE)/LICENSE

#install_man:
#	$(INSTALL) -D --mode=0644 share/man/bootless.1 $(DESTDIR)/$(PREFIX)/share/man/man1/bootless.1

#install_completion:
#	$(INSTALL) -D --mode=0644 lib/bootless/completions/bash/bootless $(DESTDIR)/$(PREFIX)/share/bash-completion/completions/bootless
#	$(INSTALL) -D --mode=0644 lib/bootless/completions/zsh/_bootless $(DESTDIR)/$(PREFIX)/share/zsh/vendor-completions/_bootless

install: clean
	@make install_lib install_bin install_doc# install_man install_completion

build_man:
	# Pipe output to sed to avoid http://lintian.debian.org/tags/hyphen-used-as-minus-sign.html
	# Fixed in http://johnmacfarlane.net/pandoc/releases.html#pandoc-1.10-2013-01-19
	pandoc -s -w man share/man/bootless.1.mdwn -o share/man/bootless.1
	sed -i -e 's/--/\\-\\-/g' share/man/bootless.1

tarball:
	mkdir -p $(ARCHIVE)
	git archive --prefix=bootless-$(VERSION)/ --format=tar HEAD | bzip2 > $(ARCHIVE)/bootless-$(VERSION).tar.bz2

release:
	@make build_man
	git commit -a -m "bootless $(VERSION)"
	# See https://github.com/nvie/gitflow/issues/87
	#     https://github.com/nvie/gitflow/pull/160
	#     https://github.com/nvie/gitflow/issues/50
	#git flow release finish -s -m "bootless $(VERSION)" $(VERSION)
	git flow release finish -s $(VERSION)
	git checkout master
	@make tarball
	gpg --use-agent --armor --detach-sign --output $(ARCHIVE)/bootless-$(VERSION).tar.bz2.asc $(ARCHIVE)/bootless-$(VERSION).tar.bz2
	scp $(ARCHIVE)/bootless-$(VERSION).tar.bz2* bootless:/var/sites/bootless/releases/
	# We're doing tagging afterwards:
	# http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=568375
	#git tag -s $(VERSION) -m "bootless $(VERSION)"
	git checkout develop

debian:
	git checkout debian
	git-import-orig --upstream-vcs-tag=$(VERSION) $(ARCHIVE)/bootless-$(VERSION).tar.bz2
	# Fine tune debian/changelog prepared by git-dch
	dch -e
	git commit -a -m "Updating debian/changelog"
	git-buildpackage --git-tag-only --git-sign-tags

web:
	@ikiwiki --setup ikiwiki.yaml

web_deploy:
	@rsync -avz --delete www/ bootless:/var/sites/bootless/www/

publish: web web_deploy
