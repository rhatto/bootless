[[!meta title="Bootless: TODO"]]

## Add support for Full Disk Encryption with LUKS version 2 with Argon2id

### About

This is a major task that need to be evaluated, since currently (as of
2024-07-13) [GNU Grub][] does not natively supports LUKS 2 and Argon2id.

This can be either implemented as a patch with a custom [GNU Grub][]
build, or wait for an official implementation.

### Status

* As of 2024-07-13, the GRUB packages for Debian bookworm still does not have
  support for luks2 and argon2id:
  [GNU GRUB - Bugs: bug #55093, Add LUKS2 support](https://savannah.gnu.org/bugs/?55093).
* This means that Full Disk Encryption won't work along with luks2 and argon2id
  when using vanilla GRUB packages from Debian.
* But it's now possible to migrate non-boot volumes.

### References

References on LUKS 2 and Argon2id are available in the [references](/references)
page.

[GNU Grub]: https://www.gnu.org/software/grub/
